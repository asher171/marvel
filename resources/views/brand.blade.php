
@extends('header')
@extends('sidebar')

@section('content')



    <div id="page-wrapper" >
        <div class="header">
            <h1 class="page-header">
                Tables Page <small>Responsive tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data</li>
            </ol>

        </div>

        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        Advanced Tables
                    </div>
                    <form action="{{ route('brandMultiple.delete') }}" method="post">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="panel-body">
                            <div class="table-responsive">

                                <div class="toolbar">

                                    <h3 class="sub-heading pull-left"><a class="btn btn-success btn-sm" href="{{ url('export') }}">Export</a> <span class="label">(Export record to csv)</span></h3>

                                    <ul class="pull-right">
                                        <li><a class="addBrand btn btn-info btn-sm"><i class="fa fa-plus"></i> Add Brand</a></li>
                                        <li><button id="sel_del" class="btn btn-danger btn-sm"  type="submit"><i class="fa fa-trash-o"></i> Delete</button></li>


                                    </ul>


                                </div>

                                <div class="add-brand-lg half">
                                    <div class="form-feild">
                                        <div class="col-md-12 col-sm-12 col-xs-12">


                                            <div class="input-group">
                                                <span class="input-group-addon"><label>Name<span style="color:#f09b22">*</span>:</label></span>

                                                <input type="text" class="form-control brand" placeholder="Add Brand">
                                                <span class="input-group-btn">
        <button class="btn btn-default AddBrand" type="button">Go!</button>
      </span>
                                            </div>
                                        </div>



                                    </div>



                                </div>

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>SNO</th>
                                        <th>Name</th>
                                        <th>Actions</th>

                                    </tr>

                                    </thead>
                                    <tbody>

                                    @foreach($data as $i => $row)
                                        <tr>
                                            <td>{{++$i}}
                                                <input type="checkbox" name="del[]" value="{{ $row->id }}">
                                            </td>
                                            <td>{{$row->name}}</td>
                                            <td><a class="btn btn-primary btn-sm" href="{{ url('brandEdit').'/'.Crypt::encrypt($row->id) }}"><i class="fa fa-edit"></i>Edit</a>
                                                | <a class="btn btn-danger btn-sm" href="{{ url('brandDelete').'/'.Crypt::encrypt($row->id) }}"><i class="fa fa-trash-o"></i>Delete</a></td>


                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </form>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
        <!-- /. ROW  -->


    </div>
    <footer><p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez.com</a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>

@endsection
<!-- /. PAGE WRAPPER  -->
@extends('footer')

<!-- Custom Js -->

</script>