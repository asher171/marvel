@section('footer')
</div>
<!-- /. WRAPPER  -->
<!-- JS Scripts-->
<!-- jQuery Js -->
{{-- @stack('js')  --}}
<script src="{{ URL::asset('assets/js/jquery-1.10.2.js') }}"></script>
<!-- Bootstrap Js -->
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>

<!-- Metis Menu Js -->
<script src="{{ URL::asset('assets/js/jquery.metisMenu.js') }}"></script>
<!-- Morris Chart Js -->
<script src="{{ URL::asset('assets/js/morris/raphael-2.1.0.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/morris/morris.js') }}"></script>


<script src="{{ URL::asset('assets/js/easypiechart.js') }}"></script>
<script src="{{ URL::asset('assets/js/easypiechart-data.js') }}"></script>

<script src="{{ URL::asset('assets/js/Lightweight-Chart/jquery.chart.js') }}"></script>

<!-- Custom Js -->

<script src="{{ URL::asset('assets/js/dataTables/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('assets/js/dataTables/dataTables.bootstrap.js') }}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/main.js') }}"></script>

<script>
    $(document).ready(function () {

       // $('#dataTables-example').dataTable();


       /* $( ".datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });*/


    });
</script>

<!-- Custom Js -->
<script src="{{ URL::asset('assets/js/custom-scripts.js') }}"></script>
</body>

</html>
@endsection