{{-- {{  @print_r($items) }} --}}
{{-- {{  dd($items)  }} --}}
@extends('add_bucket')

@section('form')
    {!! Form::open(['route'=>['Bucket.update',$data->id],'files'=>true])  !!}


 @endsection

@section('brand')


    {!! Form::select('brand', $brand, $data->brand_id, ['class' => 'form-control select_brand']) !!}

@endsection

@section('name')
{!! Form::text('name',$data->name, ['class' => 'form-control']) !!}

@endsection

@section('code')
{{--
    <input type="text" name="code" class="form-control"  value="{{ $data->code  }}">
--}}
{!! Form::text('code',$data->code, ['class' => 'form-control']) !!}

@endsection

@section('bucket')
    {!! Form::text('bucket',$data->bucket, ['class' => 'form-control']) !!}


@endsection
@section('gallon')

    {!! Form::text('gallon',$data->gallon, ['class' => 'form-control']) !!}

@endsection



@section('quality')
    {!! Form::select('quality', $color_quality, $data->quality_id , ['class' => 'form-control']) !!}

@endsection
