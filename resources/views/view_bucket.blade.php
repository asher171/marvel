@extends('header')
@extends('sidebar')

@section('content')

    <div id="page-wrapper" >
        <div class="header">
            <h1 class="page-header">
                Tables Page <small>Responsive tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data</li>
            </ol>

        </div>

        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Advanced Tables
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                <strong>Success! </strong>
                                {{ Session::get('success')}}

                            </div>
                        @endif
{{--
                        <form action="{{  route('delete') }}" method="delete">
--}}



                        <div class="panel-body">
                                <div class="toolbar">

                                    <h3 class="sub-heading pull-left"><a class="btn btn-success btn-sm" href="{{ url('export_buckets?search='.$srch['s'].'&fromDate='.$srch['fd'].'&toDate='.$srch['td']) }}">Export</a> <span class="label">(Export record to csv)</span></h3>

                                    <ul class="pull-right">

                                        <li>
                                            <form action="{{ route('viewBucket') }}">
                                            <label>Search:</label>
                                            <input type="text" placeholder="Search" name="search" id="from" class="form-control" style="height: 30px;width: auto;display: inline-block;" value="{{ $srch['s'] }}">

                                            {{--
                                               <form action="{{ route('viewBucket') }}" method="get">


                                            <div class="input-group add-on" style="width: 350px;margin-top: -2px;">
                                                 <input class="form-control" placeholder="Search" name="search" id="srch-term" type="text" value="{{ $srch['q'] }}">
                                                 <div class="input-group-btn">
                                                     <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                                 </div>
                                             </div>
                                             --}}
                                             {{--   <label>Search:</label>
                                                <input type="text" name="search" id="from" class="form-control" style="height: 30px;width: auto;display: inline-block;">--}}

{{--
                                            </form>
--}}
                                        </li>
                                        <li>


                                                <label>Date From:</label>
                                                    <input type="text" name="fromDate" id="frm" class=" datepicker form-control" style="height: 30px;width: auto;display: inline-block;" value="{{ $srch['fd'] }}">
                                                <label>To:</label>
                                                    <input type="text" name="toDate" id="to" class=" datepicker form-control" style="height: 30px;width: auto;display: inline-block;" value="{{ $srch['td'] }}">

                                                <button id="date" class="btn btn-success btn-sm" type="submit">Search</button>
                                            </form>
                                        </li>

                                        <li>
                                            <button id="sel_del" class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                                        </li>


                                    </ul>


                                </div>
                            <form action="{{ route('bucketMultiple.delete') }}" method="post">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>


                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>SNo</th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Bucket</th>
                                        <th>Brand</th>
                                        <th>Quality</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key => $row)
                                        <tr>
                                            <td>{{ ++$key }}
                                                <input type="checkbox" name="del[]" value="{{ $row->cid }}">

                                            </td>
                                            <td>{{$row->color}}</td>
                                            <td>{{$row->code}}</td>
                                            <td>{{$row->bucket}}</td>
                                            <td>{{$row->brand}}</td>
                                            <td>{{$row->quality}}</td>
                                            <td>{{$row->date}}</td>
                                            <td>
{{--
                                                {!! Form::open(['route'=>['Color_shade.delete',$row->cid],'method'=>'post']) !!}
--}}
                                                <a class="btn btn-primary btn-sm" href="{{  url('bucketEdit').'/'. Crypt::encrypt($row->cid)}}"><i class="fa fa-edit"></i>Edit</a>
                                                <span style="color: #f09b22 ">| </span>
                                                <a class="btn btn-danger btn-sm" href="{{ url('bucketDelete').'/'. Crypt::encrypt($row->cid) }}"><i class="fa fa-trash-o"></i>Delete</a>
                                               {{-- {!! Form::button('Delete',['class'=>'btn btn-danger btn-sm','type'=>'submit']) !!}
                                                {!! Form::close() !!}--}}
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </form>
                            <span class="col-sm-4">
                            Showing {{ $data->currentPage() }} to {{ $data->count() }} of {{ $data->total() }} entries
                        </span>
                            <span class="col-sm-4">
                            </span>
                            <span class="col-sm-4">
                            {{ $data->appends(Request::except('page'))->links() }}
                            {{--@if(isset($srch['s']) and !empty($srch['s']))
                                {{ $data->appends(['search' => $srch['s']])->links() }}
                            @else
                                    {{ $data->links() }}
                            @endif--}}

                        </span>
                            </div>



                    </div>

{{--
                        </form>
--}}
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
            <!-- /. ROW  -->


        </div>
        <footer><p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez.com</a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>

@endsection
<!-- /. PAGE WRAPPER  -->
@extends('footer')

<!-- Custom Js -->
