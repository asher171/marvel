@extends('header')
@extends('sidebar')

@section('content')

    <div id="page-wrapper" >
        <div class="header">
            <h1 class="page-header">
                Tables Page <small>Responsive tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Data</li>
            </ol>

        </div>

        <div id="page-inner">

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Advanced Tables
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                <strong>Success! </strong>
                                {{ Session::get('success')}}

                            </div>
                        @endif
                        {{--
                                                <form action="{{  route('delete') }}" method="delete">
                        --}}

                        <form action="{{ route('gallonMultiple.delete') }}" method="post">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>


                            <div class="panel-body">
                                <div class="toolbar">

                                    <h3 class="sub-heading pull-left"><a class="btn btn-success btn-sm" href="{{ url('export_gallons') }}">Export</a> <span class="label">(Export record to csv)</span></h3>

                                    <ul class="pull-right">
                                        <li>
                                            <button id="sel_del" class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                                        </li>


                                    </ul>


                                </div>
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>SNo</th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Gallon</th>
                                        <th>Brand</th>
                                        <th>Created Date</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key => $row)
                                        <tr>
                                            <td>{{ ++$key }}
                                                <input type="checkbox" name="del[]" value="{{ $row->cid }}">

                                            </td>
                                            <td>{{$row->color}}</td>
                                            <td>{{$row->code}}</td>
                                            <td>{{$row->gallon}}</td>
                                            <td>{{$row->brand}}</td>
                                            <td>{{$row->date}}</td>
                                            <td>
                                                {{--
                                                    {!! Form::open(['route'=>['Color_shade.delete',$row->cid],'method'=>'post']) !!}
                                                --}}
                                                <a class="btn btn-primary btn-sm" href="{{  url('gallonEdit').'/'. Crypt::encrypt($row->cid)}}"><i class="fa fa-edit"></i>Edit</a>
                                                <span style="color: #f09b22 ">| </span>
                                                <a class="btn btn-danger btn-sm" href="{{ url('gallonDelete').'/'. Crypt::encrypt($row->cid) }}"><i class="fa fa-trash-o"></i>Delete</a>
                                                {{-- {!! Form::button('Delete',['class'=>'btn btn-danger btn-sm','type'=>'submit']) !!}
                                                 {!! Form::close() !!}--}}
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                <span class="col-sm-4">
                            Showing {{ $data->currentPage() }} to {{ $data->count() }} of {{ $data->total() }} entries
                        </span>
                                <span class="col-sm-4">
                            </span>
                                <span class="col-sm-4">
                            {{ $data->links() }}
                        </span>


                            </div>
                        </form>
                    </div>
                    {{--
                                            </form>
                    --}}
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
        <!-- /. ROW  -->


    </div>
    <footer><p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez.com</a></p></footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>

@endsection
<!-- /. PAGE WRAPPER  -->
@extends('footer')

<!-- Custom Js -->
