@extends('header')
@extends('sidebar')

@section('content')

    <div id="page-wrapper" >
        <div class="header">
            <h1 class="page-header">
                Forms Page <small>Best form elements.</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Data</li>
            </ol>

        </div>

        <div id="page-inner">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Basic Form Elements
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            <strong>Success! </strong>
                                            {{ Session::get('success')}}

                                        </div>
                                    @endif


                                        {!! Form::open(['route'=>['quality.update',$data->id],'files'=>true]) !!}

                                    <div class="form-group">
                                        {!! Form::label('Quality') !!}
                                        {!! Form::text('name',$data->name, ['class' => 'form-control']) !!}


                                    </div>
                                    <?php
                                    echo $errors->first('quality','<div class="alert alert-danger">
                                         <strong>ERROR! </strong>:message</div>');

                                    ?>
                                        {!! Form::submit('Update') !!}
                                        {!! Form::close()!!}
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <footer><p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez.com</a></p></footer>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->

@endsection
@extends('footer')