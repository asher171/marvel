{{-- {{  @print_r($items) }} --}}
{{-- {{  dd($items)  }} --}}
@extends('add_gallon')

@section('form')
    {!! Form::open(['route'=>['Gallon.update',$data->id],'files'=>true])  !!}


@endsection

@section('brand')


    {!! Form::select('brand', $items, $data->brand_id, ['class' => 'form-control']) !!}

@endsection

@section('name')
    {{--
        <input type="text" name="name" class="form-control"  value="{{ $data->name  }}">
    --}}
    {!! Form::text('name',$data->name, ['class' => 'form-control']) !!}

@endsection

@section('code')
    {{--
        <input type="text" name="code" class="form-control"  value="{{ $data->code  }}">
    --}}
    {!! Form::text('code',$data->code, ['class' => 'form-control']) !!}

@endsection

@section('gallon')

    {!! Form::text('gallon',$data->gallon, ['class' => 'form-control']) !!}

@endsection


