<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BucketRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' =>'required',
            'name' =>'required',
            'code' =>'required|unique:buckets,code',
            'bucket' =>'required|integer'
        ];
    }

    public function messages()
    {
        return[
          'brand.required' =>'must select the :attribute',
          'name.required' =>'must fill the :attribute',
          'code.required' =>'must fill the :attribute',
          'code.unique' => ':attribute already saved in database',
          'bucket.required' =>'must fill the :attribute',
          'bucket.integer' =>':attribute must be an integer value',
        ];
    }
}
