<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'alpha_spaces|required',
            'email' =>'required|email|unique:customers,email',
            'phone' =>'Numeric|required|digits_between:11,11',
            'address' =>'required',
            'color' =>'required',
            'doc_image' =>'required|image|mimes:jpg,jpeg',
            //'password' =>'required',
            //'conf_password' =>'required|same:password'


        ];
    }

    public function messages()
    {
        return [
          'name.required' => 'must fill this field',
          'name.alpha_spaces' => 'this field must be in letter',
          'email.required' => 'must fill this field',
          'email.email' => 'Enter Correct Email',
          'email.unique' => ':attribute already saved in database',
          'phone.required' => "must fill the :attribute  field",

         // 'phone.min' => ":attribute number minimum have :min digits",
          //'phone.max' => ":attribute number maximum have :max digits",
          'phone.digits_between' => ":attribute number maximum have 11 digits",
          'address.required' => "must fill the :attribute  field",
          'color.required' => "must fill the :attribute  field",
          'doc_image.required' =>"must upload the image",
          'doc_image.image' =>"only upload the image",
          'doc_image.mimes' => "only allowed type of image is 'jpg,jpeg'"

          //'password.required' => 'must fill this field',
          //'conf_password.required' => 'must fill this field',
          //'conf_password.same' => 'The confirmation does not match'

        ];

    }
}
