<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormRequest;
use Illuminate\Http\Request;
use App\Http\Requests\BucketRequest;

use Illuminate\Support\Facades\Input;

use App\Color_quality;
use App\Bucket as ColorShade;
use App\brand;
use DB;

class ColorShades extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //$data = DB::table('color_qualities')->get();
        $items = brand::lists('name','id')->toArray();
       // $items['0'] = 'Select Brand';


        //$items = color_quality::lists('name','id');
        //$items = DB::table('color_qualities')->lists('id','name');
        return view('add_shade',compact('items'));
    }

    public function ShadeSubmit(BucketRequest $request)
    {
       // print_r(Input::all());
       if($request->has('name')) {

           $query = new ColorShade([
               'brand_id' => $request->brand,
               'quality_id' => $request->quality,
               'name' => $request->name,
               'code' => $request->code,
               'bucket' => $request->bucket
           ]);

           $query->save();
           session()->flash('success','Your data has been added');
           return redirect('addShades');

       }
    }
    public function viewShades(){

       // $data = color_Shade::get();
       $data = DB::table('color_shades as c')
            ->select('c.id as cid','c.name as color','c.code as code','c.bucket as bucket','c.gallon as gallon','c.created_at as date','q.id as qid','q.name as quality')
           ->join('color_qualities as q','c.quality_id', '=', 'q.id')
           ->orderByRaw('date ASC')
           ->get();
        return view('view_shades',compact('data'));
    }

    function delete($id){
         //return $id;
       // dd($color_Shade);
        //DB::table('color_shades')->delete(7,6);

        $resource = ColorShade::find(decrypt($id));
        $resource->delete();


        return  redirect('viewShades');
    }
    function multipleDelete(Request $request){

        DB::table('color_shades')->whereIn('id', $request->del)->delete();
        return  redirect('viewShades');
    }

    function edit($id){
        //return $id;
        // dd($color_Shade);
        $data = ColorShade::find(decrypt($id));
        $items = color_quality::lists('name','id');
        return view('shadeEdit',compact('data','items'));

    }

    function update(Request $request,$id)
    {
       $this->validate($request,[
           'name' =>'required',
           'code' =>'required'
       ]);

        ColorShade::where('id', $id)
            ->update([
                'quality_id' => $request->quality,
                'name' => $request->name,
                'code' => $request->code,
                'bucket' => $request->bucket,
                'gallon' => $request->gallon
            ]);
        session()->flash('success','Your data has been updated');
        return  redirect('viewShades');



    }
    function GetQuality(Request $request)
    {
        $data =Color_quality::where('brand_id', $request->brand)->pluck('name','id');
        // dd($data);
       if($data->count() > 0)
       {
          // echo "hi";
            echo '<select name="quality" class="form-control">';
            foreach($data as $k => $b):
                echo '<option value="'.$k.'">'.$b.'</option>';
            endforeach;
             echo '</select>';
       }
       else
           echo "Not Found";
    }


}
