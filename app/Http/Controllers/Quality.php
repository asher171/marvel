<?php

namespace App\Http\Controllers;

use Faker\Provider\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Color_quality;
use App\brand;
use DB;

class Quality extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){

//        $data = DB::table('color_qualities')->get();

        $data = DB::table('color_qualities as c')
            ->select('c.id as id','c.name as name','c.created_at as date','b.id as bid','b.name as brand')
            ->join('brands as b','b.id','=','c.brand_id')
            ->get();
        $brand = brand::pluck('name','id');
        return view('quality',compact('data','brand'));

    }
    public function addQuality(Request $request){

       /**
        * THIS ALL FUNCTIONS HAS WORKING
        */
        // print_r($_POST);
        //print_r(Input::all());
        //echo  $request->input( 'quality' );
      // echo INPUT::get('quality');
        //echo $request->quality;


        /**
         * NOW SAVE THE RECORD IF IT'S NOT NULL
         */

       if($request->has('quality')) {

           $quality = new Color_quality([
               'name' => $request->quality,
               'brand_id' => decrypt($request->brand)

           ]);

           $quality->save();
       }
    }

    public function edit($id){
        $data = Color_quality::find(decrypt($id));
        return view('qualEdit',compact('data'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
        ]);

        Color_quality::where('id',$id)
            ->update([
                'name'=> $request->name
            ]);
        session()->flash('success','Your data has been updated');
        return redirect('quality');
    }

    public function delete($id){
        $del =Color_quality::find(decrypt($id));
        $del->delete();

        return redirect('quality');

    }
    function multipleDelete(Request $request){

        DB::table('color_qualities')->whereIn('id', $request->del)->delete();
        return  redirect('quality');
    }


}

