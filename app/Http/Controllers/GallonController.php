<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\GallonRequest;

use App\Color_quality;
use App\brand;
use App\Gallon;
use DB;

class GallonController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $items = brand::pluck('name','id')->toArray();
         return view('add_gallon',compact('items'));
    }

    public function GallonSubmit(GallonRequest $request)
    {
        // print_r(Input::all());
        if($request->has('name')) {

            $query = new Gallon([
                'brand_id' => $request->brand,
                'name' => $request->name,
                'code' => $request->code,
                'gallon' => $request->gallon
            ]);

            $query->save();
            session()->flash('success','Your data has been added');
            return redirect('addGallon');

        }
    }
    public function viewGallon(){

        // $data = color_Shade::get();
        $data = DB::table('gallons as c')
            ->select('c.id as cid','c.name as color','c.code as code','c.gallon as gallon','c.created_at as date','b.id as bid','b.name as brand')
            ->join('brands as b','c.brand_id', '=', 'b.id')
            ->orderByRaw('date ASC')
            ->paginate(1);
        return view('view_gallon',compact('data'));
    }

    function delete($id){
        //return $id;
        // dd($color_Shade);
        //DB::table('color_shades')->delete(7,6);

        $resource = Gallon::find(decrypt($id));
        $resource->delete();


        return  redirect('viewGallon');
    }
    function multipleDelete(Request $request){

        DB::table('gallons')->whereIn('id', $request->del)->delete();
        return  redirect('viewGallon');
    }

    function edit($id){
        //return $id;
        // dd($color_Shade);
        $data = Gallon::find(decrypt($id));
        $items = brand::pluck('name','id')->toArray();
        return view('gallonEdit',compact('data','items'));

    }

    function update(Request $request,$id)
    {
        $this->validate($request,[
            'name' =>'required',
            'code' =>'required',
            'brand' =>'required',
            'gallon' =>'required'
        ]);

        Gallon::where('id', $id)
            ->update([
                'brand_id' => $request->brand,
                'name' => $request->name,
                'code' => $request->code,
                'gallon' => $request->gallon
            ]);
        session()->flash('success','Your data has been updated');
        return  redirect('viewGallon');



    }

}
