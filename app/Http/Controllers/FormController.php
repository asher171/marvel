<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FormRequest;

use App\Customer;
use DB;

class FormController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');
    }
    //
    function formSubmit(FormRequest $request){
        //echo "hi";

      /*  $this->validate($request,[
            'text1' =>'alpha|required',
            'text2' =>'required'
        ]);*/

//        echo $request->name;
//        echo $request->email;
//        echo $request->password;
//
       //$customers = Customer::all();
       //dd($customers);

  /*      $input = Input::all();
        $cus = new Customer($input);*/

       /* $customer = new Customer;
        $customer->name =$request->name;
        $customer->email =$request->email;
        $customer->password =$request->password;

        $customer->save();*/



        $customer = new Customer([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'color' => $request->color,

            //'password' => $request->password,
        ]);

        $customer->save();

        session()->flash('success','Your data has been added');
        return redirect('form');
    }

    function getData(Request $request){
        $data = DB::table('Customers')
                ->select(['name','email','phone','address','color'])
                ->get();
        return view('table',compact('data'));//->with('data',data);
    }

    public function download_csv(Request $request){
          $query = DB::table('Customers')
            ->select(['name','email','phone','address','color'])
            ->get();
          $tot_record_found=0;
        if(count($query)>0){
            $tot_record_found=1;
            //First Method

            $export_data="Name,Email,phone,Address,Color\n";
            foreach($query as $value){
                $export_data.=$value->name.','.$value->email.','.$value->phone.','.$value->address.$value->color."\n";
            }
            $filename=date('Y-m-d h-i-s').".csv";
            //export using response method

            return response($export_data)
                ->header('Content-Type','application/csv')
                ->header('Content-Disposition', 'attachment; filename="'.$filename.'"')
                ->header('Pragma','no-cache')
                ->header('Expires','0');




            //export using download method


            /*$CsvData=array('Name,Email,phone,Address,Color');
            foreach($query as $value){
                $CsvData[]=$value->name.','.$value->email.','.$value->phone.','.$value->address.$value->color;
            }

            $filename=date('Y-m-d h-i-s').".csv";
            $file_path=base_path().'/'.$filename;
            $file = fopen($file_path,"w+");
            foreach ($CsvData as $exp_data){
                fputcsv($file,explode(',',$exp_data));
            }
            fclose($file);

            $headers = ['Content-Type' => 'application/csv'];
            return response()->download($file_path,$filename,$headers );*/
        }
        return view('download_csv',['record_found' =>$tot_record_found]);




    }
}
