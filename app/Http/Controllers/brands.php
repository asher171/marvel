<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\brand;
use DB;
class brands extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    public function index(){

        //$data = DB::table('brands')->get();
        $data = brand::all(['id','name']);
        return view('brand',compact('data'));

    }

    public function addBrand(Request $request){

        /**
         * THIS ALL FUNCTIONS HAS WORKING
         */
        // print_r($_POST);
        //print_r(Input::all());
        //echo  $request->input( 'quality' );
        // echo INPUT::get('quality');
        //echo $request->quality;


        /**
         * NOW SAVE THE RECORD IF IT'S NOT NULL
         */

        if($request->has('brand')) {

            $quality = new brand([
                'name' => $request->brand,

            ]);

            $quality->save();
        }
    }

    public function edit($id){
        $data = brand::find(decrypt($id));
        return view('brandEdit',compact('data'));
    }

    public function update(Request $request,$id){
        $this->validate($request,[
            'name'=>'required',
        ]);

        brand::where('id',$id)
            ->update([
                'name'=> $request->name
            ]);
        session()->flash('success','Your data has been updated');
        return redirect('brand');
    }

    public function delete($id){
        $del =brand::find(decrypt($id));
        $del->delete();

        return redirect('brand');

    }
    function multipleDelete(Request $request){

        DB::table('brands')->whereIn('id', $request->del)->delete();
        return  redirect('brand');
    }
}
