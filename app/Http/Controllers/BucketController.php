<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormRequest;
use Illuminate\Http\Request;
use App\Http\Requests\BucketRequest;

use Illuminate\Support\Facades\Input;

use App\Color_quality;
use App\Bucket as Bucket;
use App\brand;
use DB;
use Excel;
use Psy\Formatter\Formatter;

class BucketController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //$data = DB::table('color_qualities')->get();
        $brand = brand::lists('name','id')->toArray();
        // $items['0'] = 'Select Brand';


        //$items = color_quality::lists('name','id');
        //$items = DB::table('color_qualities')->lists('id','name');
        return view('add_bucket',compact('brand'));
    }

    public function BucketSubmit(BucketRequest $request)
    {
        // print_r(Input::all());
        if($request->has('name')) {

            $query = new Bucket([
                'brand_id' => $request->brand,
                'quality_id' => $request->quality,
                'name' => $request->name,
                'code' => $request->code,
                'bucket' => $request->bucket
            ]);

            $query->save();
            session()->flash('success','Your data has been added');
            return redirect('addBucket');

        }
    }
    public function viewBucket(Request $request){
       // $abc = (isset($request->search)) ? ['q' => $request->search] : ['q'=>''];
        // echo $request->searchName;
           // exit;
        // $data = color_Shade::get();
        $data = DB::table('buckets as c')
            ->select('c.id as cid','c.name as color','c.code as code','c.bucket as bucket','c.created_at as date','b.id as bid','b.name as brand','q.id as qid','q.name as quality')
            ->join('color_qualities as q','c.quality_id', '=', 'q.id')
            ->join('brands as b','c.brand_id', '=', 'b.id')
            ->orderByRaw('date ASC');
        if(isset($request->search) and !empty($request->search))
        {
             $srch = array();
               /*
                $data = $data->where('c.name', 'LIKE', $request->search .'%')
                ->orWhere('c.code', 'like', $request->search  . '%')
                ->orWhere('c.bucket', 'like', $request->search  . '%')
                ->orWhere('b.name', 'like', $request->search  . '%')
                ->orWhere('q.name', 'like',  $request->search  . '%');
*/
            // echo  Input::get('search');
            $data = $data->where(function ($query) {
                $query->where('c.name', 'LIKE', Input::get('search') .'%')
                    ->orWhere('c.code', 'like', Input::get('search')  . '%')
                    ->orWhere('c.bucket', 'like', Input::get('search')  . '%')
                    ->orWhere('b.name', 'like', Input::get('search')  . '%')
                    ->orWhere('q.name', 'like',  Input::get('search')  . '%');
            });
            $srch['s' ] =  $request->search;
        }
        else
            $srch['s'] = '';
       if(isset($request->fromDate) and !empty($request->fromDate)) {
            $data = $data->where('c.created_at', '>=', $request->fromDate . ' 00:00:00');
            $srch['fd' ] = $request->fromDate;

            if (isset($request->toDate) and !empty($request->toDate)) {

                $data = $data->where('c.created_at', '<=', $request->toDate . ' 23:59:59');
                $srch['td' ] = $request->toDate;

            }
        }
        else if(isset($request->toDate) and !empty($request->toDate)) {
            $data = $data->where('c.created_at', '<=', $request->toDate . ' 23:59:59');
            $srch['td' ] = $request->toDate;
            if (isset($request->fromDate) and !empty($request->fromDate)) {

                $data = $data->where('c.created_at', '=>', $request->fromDate . ' 00:00:00');
                $srch['fd' ] = $request->fromDate;
            }
        }
        else
        {
            //$srch = ['fd' => '','td' => ''];
            $srch['fd'] = '';
            $srch['td'] = '';
        }
        $data = $data->paginate(1);
      /*if(Input::has('export'))
      {
          $export_data="Name,Code,Bucket,Brand,Quality,Crated Date\n";
          foreach($data as $value){
              $export_data.=$value->color.','.$value->code.','.$value->bucket.','.$value->brand.$value->quality.$value->date."\n";
          }
          $filename=date('Y-m-d h-i-s').".csv";
          //export using response method
          $export_data = redirect('viewBucket');
          return response($export_data)
              ->header('Content-Type','application/csv')
              ->header('Content-Disposition', 'attachment; filename="'.$filename.'"')
              ->header('Pragma','no-cache')
              ->header('Expires','0');

      }
      else*/
        return view('view_bucket',compact('data','srch'));
    }

    function delete($id){
        //return $id;
        // dd($color_Shade);
        //DB::table('color_shades')->delete(7,6);

        $resource = Bucket::find(decrypt($id));
        $resource->delete();


        return  redirect('viewBucket');
    }
    function multipleDelete(Request $request){

        DB::table('buckets')->whereIn('id', $request->del)->delete();
        return  redirect('viewBucket');
    }

    function edit($id){
        //return $id;
        // dd($color_Shade);
        $brand = brand::lists('name','id')->toArray();
        $data = Bucket::find(decrypt($id));
        $color_quality= color_quality::where('brand_id', $data->brand_id)->lists('name','id')->toArray();
        return view('BucketEdit',compact('data','color_quality','brand'));

    }

    function update(Request $request,$id)
    {
        $this->validate($request,[
            'name' =>'required',
            'code' =>'required',
            'brand_id' =>'required',
            'bucket' =>'required'
        ]);

        Bucket::where('id', $id)
            ->update([
                'quality_id' => $request->quality,
                'brand_id' => $request->brand,
                'name' => $request->name,
                'code' => $request->code,
                'bucket' => $request->bucket,
                //'gallon' => $request->gallon
            ]);
        session()->flash('success','Your data has been updated');
        return  redirect('viewBucket');



    }
    function GetQuality(Request $request)
    {
        $data =Color_quality::where('brand_id', $request->brand)->pluck('name','id');
        // dd($data);
        if($data->count() > 0)
        {
            // echo "hi";
            echo '<select name="quality" class="form-control">';
            foreach($data as $k => $b):
                echo '<option value="'.$k.'">'.$b.'</option>';
            endforeach;
            echo '</select>';
        }
        else
            echo "Not Found";
    }


    function exportBucket(Request $request){

        $data = DB::table('buckets as c')
            ->select('c.id as cid','c.name as color','c.code as code','c.bucket as bucket','c.created_at as date','b.id as bid','b.name as brand','q.id as qid','q.name as quality')
            ->join('color_qualities as q','c.quality_id', '=', 'q.id')
            ->join('brands as b','c.brand_id', '=', 'b.id')
            ->orderByRaw('date ASC');
        if(isset($request->search) and !empty($request->search))
        {
            $srch = array();
            $data = $data->where(function ($query) {
                $query->where('c.name', 'LIKE', Input::get('search') .'%')
                    ->orWhere('c.code', 'like', Input::get('search')  . '%')
                    ->orWhere('c.bucket', 'like', Input::get('search')  . '%')
                    ->orWhere('b.name', 'like', Input::get('search')  . '%')
                    ->orWhere('q.name', 'like',  Input::get('search')  . '%');
            });
            $srch['s' ] =  $request->search;
        }
        else
            $srch['s'] = '';
        if(isset($request->fromDate) and !empty($request->fromDate)) {
            $data = $data->where('c.created_at', '>=', $request->fromDate . ' 00:00:00');
            $srch['fd' ] = $request->fromDate;

            if (isset($request->toDate) and !empty($request->toDate)) {

                $data = $data->where('c.created_at', '<=', $request->toDate . ' 23:59:59');
                $srch['td' ] = $request->toDate;

            }
        }
        else if(isset($request->toDate) and !empty($request->toDate)) {
            $data = $data->where('c.created_at', '<=', $request->toDate . ' 23:59:59');
            $srch['td' ] = $request->toDate;
            if (isset($request->fromDate) and !empty($request->fromDate)) {

                $data = $data->where('c.created_at', '=>', $request->fromDate . ' 00:00:00');
                $srch['fd' ] = $request->fromDate;
            }
        }
        else
        {
            //$srch = ['fd' => '','td' => ''];
            $srch['fd'] = '';
            $srch['td'] = '';
        }
        $data = $data->paginate(1)->toArray();
       // $data = Bucket::get()->toArray();
        /*$srch['s'] = $request->search;
        $srch['fd'] = $request->fromDate;
        $srch['td'] = $request->toDate;*/

       // $data2 = (array)$data['data'];
        $data2 = json_decode( json_encode($data['data']), true);
       //  print_r($data2);
       // dd($data2);

       // $data2=array(1,2,3,4);
        Excel::create('New file', function($excel) use($srch,$data2) {

            $excel->sheet('New sheet', function($sheet) use($srch,$data2) {
                $sheet->fromArray($data2);
                //$sheet->with($data);
                //$sheet->loadView('view_bucket',compact('srch','data'));

            });

        })->export('csv');
    }

}
