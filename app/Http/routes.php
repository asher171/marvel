<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::group(['middleware' =>['web']],function(){
Route::get('/', function () {
    return view('index');
})->middleware('auth');

Route::get('/dashboard', function () {
    return view('index');
});

Route::get('form', function () {
    return view('form');
})->middleware('auth');

Route::get('test', function () {
    return view('auth/login');
});

Route::get('test2', function () {
    return view('layouts/app');
});

Route::post('form-submit',['uses' =>'FormController@formSubmit',
    'as' =>'f.submit'

]);

Route::get('viewData','FormController@getData');
Route::get('export', 'FormController@download_csv');

//});

Route::auth();

Route::get('/home', 'HomeController@index');


// working for color site

/*
* Brand Controller
*/
Route::get('brand','Brands@index');
Route::post('addBrand','Brands@addBrand');
Route::get('brandEdit/{id}','Brands@edit');
Route::get('brandDelete/{id}','Brands@delete');
Route::post('brand-update/{id}',['uses'=>'Brands@update',
    'as' => 'brand.update'
]);

Route::post('brandMultiple-delete',['uses' =>'Brands@multipleDelete',
    'as' =>'brandMultiple.delete'

]);

/*
 * End Brand Controller
 */



/*
 * Quality Controller
 */
Route::get('quality','Quality@index');
Route::post('addQuality','Quality@addQuality');
Route::get('qualEdit/{id}','Quality@edit');
Route::get('qualDelete/{id}','Quality@delete');
Route::post('quality-update/{id}',['uses'=>'Quality@update',
    'as' => 'quality.update'
    ]);

Route::post('qualMultiple-delete',['uses' =>'Quality@multipleDelete',
    'as' =>'qualMultiple.delete'

]);

/*
 * End Quality Controller
 */



/*
 * Bucket Controller
 */
Route::post('Bucket-submit',['uses' =>'BucketController@BucketSubmit',
    'as' =>'bucket.submit'

]);
Route::get('addBucket','BucketController@index');
Route::get('viewBucket','BucketController@viewBucket');
Route::post('viewBucket',['users' => 'BucketController@viewBucket',
    'as'=>'viewBucket'
    ]);

Route::post('bucketMultiple-delete',['uses' =>'BucketController@multipleDelete',
    'as' =>'bucketMultiple.delete'

]);

Route::get('bucketEdit/{id}','BucketController@edit');
Route::get('bucketDelete/{id}','BucketController@delete');

Route::post('bucket-update/{id}',['uses' =>'BucketController@update',
    'as' =>'Bucket.update'

]);
Route::post('getQuality','BucketController@GetQuality');


Route::get('export_buckets', 'BucketController@exportBucket');

/*
 * End Bucket Controller
 */


/*
 * Gallon Controller
 */
Route::post('Gallon-submit',['uses' =>'GallonController@GallonSubmit',
    'as' =>'gallon.submit'

]);
Route::get('addGallon','GallonController@index');
Route::get('viewGallon','GallonController@viewGallon');

Route::post('gallonMultiple-delete',['uses' =>'GallonController@multipleDelete',
    'as' =>'gallonMultiple.delete'

]);

Route::get('gallonEdit/{id}','GallonController@edit');
Route::get('gallonDelete/{id}','GallonController@delete');

Route::post('gallon-update/{id}',['uses' =>'GallonController@update',
    'as' =>'Gallon.update'

]);

Route::get('export_gallons', 'FormController@exportGallon');

/*
 * End Gallon Controller
*/