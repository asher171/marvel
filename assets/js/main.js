$(document).ready(function () {

    $( ".datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('.addQua').click(function(e) {
        $(this).parents('.toolbar').next('.add-quality-lg').slideToggle();


    });

    $(document).on("click",".AddQuality",function(event){ // save category from 'Manage Sub Categories'
        event.preventDefault();
        var name = $(".qual").val();
        var brand = $(".brand").val();
        // alert(name);
        //exit;
        if(name == "" || brand == "0")
        {
            alert("Must fill the field")

        }
        else
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'addQuality',
                type: 'POST',
                data: {'quality':name,'brand':brand},
                success: function(data) {
                    //alert(data)
                    alert('Successfully added')
                    location.reload();
                    //$(".AdSubCat").prop('value','')
                    //$('.ShowSubCat').html(data);
                }
            })
        }

    });


    $('.addBrand').click(function(e) {
        $(this).parents('.toolbar').next('.add-brand-lg').slideToggle();


    });

    $(document).on("click",".AddBrand",function(event){ // save category from 'Manage Sub Categories'
     event.preventDefault();
        var name = $(".brand").val();
        // alert(name);
        //exit;
        if(name == "")
        {
            alert("Must fill the field")

        }
        else
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: 'addBrand',
                type: 'POST',
                data: {'brand':name},
                success: function(data) {
                    //alert(data)
                    alert('Successfully added')
                    location.reload();
                    //$(".AdSubCat").prop('value','')
                    //$('.ShowSubCat').html(data);
                }
            })
        }

    })

    $(document).on("change",".select_brand",function(){ // save category from 'Manage Sub Categories'
       // event.preventDefault();
       var local= "http://localhost/marvel/getQuality";
        var id = $(this).val();
         //alert(id);
        // exit;
        if(id == "")
        {
            alert("Must select the field")

        }
        else
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                //url: 'getQuality',
                url: local,
                type: 'POST',
                data: {'brand':id},
                success: function(data) {
                   // alert(data)
                    //$(".AdSubCat").prop('value','')
                    $('.getQuan').html(data);
                }
            })
        }

    })








})