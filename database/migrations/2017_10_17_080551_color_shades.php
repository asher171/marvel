<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ColorShades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('color_shades',function(Blueprint $table){

            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->integer('bucket')->nullable();
            $table->integer('gallon')->nullable();
            $table->integer('brand_id');
            $table->integer('quality_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('color_shades');
    }
}

